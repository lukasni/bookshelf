Bookshelf
=========

This is a simple application that allows its users to track what books they are reading and the time it takes them to complete each book.

This project is mainly to be considered a practice project, since I am using it to (re)learn the materialize.css frontend framework as well as the Flask Python microramework. I will also use it as an oportunity to learn visualizing statistical data in a web environment

Why a book database?
--------------------

Good question. With things like Goodreads out there, there isn't really any need for another application like this.  The original motivation behind the project was the death of Shelfari which offered some statistics and other functionality that was missing in Goodreads which I'm hoping to include in this application. Where Goodreads is very much focussed on the social aspect, this will be a purely personal database for nerds that want to track how many pages they read in the last month, which genre they tend to take the most time on and similar unnecessary info =)

