from app import db
from datetime import datetime


user_has_book = db.Table('user_has_book',
        db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
        db.Column('book_id', db.Integer, db.ForeignKey('book.id'))
)

class User(db.Model):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    password = db.Column(db.String())
    books = db.relationship('Book', secondary=user_has_book)

    def __init__(self, name, password):
        self.name = name
        self.password = password

    def __repr__(self):
        return '<User %r>' % self.name


book_has_author = db.Table('book_has_author',
        db.Column('author_id', db.Integer, db.ForeignKey('author.id')),
        db.Column('book_id', db.Integer, db.ForeignKey('book.id'))
)


class Author(db.Model):
    __tablename__ = 'author'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Author %r>' % self.name


class Book(db.Model):
    __tablename__ = 'book'

    id = db.Column(db.Integer, primary_key=True)
    ISBN = db.Column(db.String(13))
    title = db.Column(db.String())
    authors = db.relationship('Author', secondary=book_has_author,
            backref=db.backref('books', lazy='dynamic'))

    def __init__(self, ISBN, title):
        self.ISBN = ISBN
        self.title = title

    def __repr__(self):
        return '<Book %r>' % self.title


class ReadDate(db.Model):
    __tablename__ = 'read_date'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime)
    book_id = db.Column(db.Integer, db.ForeignKey('book.id'))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    book = db.relationship('Book')
    user = db.relationship('User')

    def __init__(self, user, book, date=None):
        self.user = user
        self.book = book
        if date is None:
            date = datetime.utcnow()
        self.date = date

    def __repr__(self):
        return '<ReadDate %r>' % self.id

